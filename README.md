# Debian Changelog Visualiser

Try:

```
./debian-changelog-visualiser /path/to/changelogs | gource --log-format custom - --logo /usr/share/pixmaps/debian-logo.png -1280x720 -c 4 -s 0.05 -o - --date-format '%B %Y' | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 gource.mp4
```

## TODO

* Group packages by section
